﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Order.Shared.Interface
{
    public interface IOrderReadOnlyDalService
    {
        Task<Result<OrderModel>> GetByIdAsync(long id);

        Task<Result<IReadOnlyCollection<OrderModel>>> GetByStatusAsync(OrderStatusType statusType, int offset, int pageSize = 100);
    }
}
