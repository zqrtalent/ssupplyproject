﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;
using System.Collections.Generic;

namespace SSupplyAssignment.EventBus.Shared
{
    public interface IEventBusSubscriptionsManager
    {
        bool IsEmpty { get; }
        event EventHandler<string> OnEventRemoved;
        void AddDynamicSubscription<TH>(string eventName)
           where TH : IDynamicEventMessageHandler;

        void AddSubscription<T, TH>()
           where T : EventMessage
           where TH : IEventMessageHandler<T>;

        void RemoveSubscription<T, TH>()
             where TH : IEventMessageHandler<T>
             where T : EventMessage;
        void RemoveDynamicSubscription<TH>(string eventName)
            where TH : IDynamicEventMessageHandler;

        bool HasSubscriptionsForEvent<T>() where T : EventMessage;
        bool HasSubscriptionsForEvent(string eventName);
        Type GetEventTypeByName(string eventName);
        void Clear();
        IEnumerable<SubscriptionInfo> GetHandlersForEvent<T>() where T : EventMessage;
        IEnumerable<SubscriptionInfo> GetHandlersForEvent(string eventName);
        string GetEventKey<T>();
    }
}
