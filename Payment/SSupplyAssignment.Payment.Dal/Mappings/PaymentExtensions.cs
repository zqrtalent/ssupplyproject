﻿using System;
using SSupplyAssignment.Payment.Shared.Models;
using Entities = SSupplyAssignment.Payment.Dal.Entities;

namespace SSupplyAssignment.Payment.Dal.Mappings
{
    public static class PaymentExtensions
    {
        public static PaymentModel MapToModel(this Entities.Payment entity)
        {
            if (entity == null) return null;
            return new PaymentModel
            {
                Id = entity.Id,
                StatusTypeCode = entity.StatusTypeCode,
                DateCreatedUTC = entity.DateCreatedUTC,
                DateLastUpdatedUTC = entity.DateLastUpdatedUTC,
                Amount = entity.Amount,
                PaymentKey = entity.PaymentKey,
                ProductItemCode  = entity.ProductItemCode,
            };
        }

        public static Entities.Payment MapToEntity(this PaymentModel model, bool includeId = true)
        {
            if (model == null) return null;
            return new Entities.Payment
            {
                Id = includeId ? model.Id : 0,
                StatusTypeCode = model.StatusTypeCode,
                DateCreatedUTC = model.DateCreatedUTC,
                DateLastUpdatedUTC = model.DateLastUpdatedUTC,
                Amount = model.Amount,
                PaymentKey = model.PaymentKey,
                ProductItemCode = model.ProductItemCode
            };
        }
    }
}
