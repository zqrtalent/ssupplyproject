﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SSupplyAssignment.Core.Shared.Models
{
	public class Result
	{
		public enum MessageType
		{
			Error = 0,
			Warning,
			Info,
			Debug
		}

		public class ResultMessage
		{
			public MessageType Type { get; set; }
			public string Message { get; set; }
		}

		private bool _success = true;
		private IList<ResultMessage> _messages;

		public void AddMessage(MessageType type, string message)
		{
			if (_messages == null)
			{
				_messages = new List<ResultMessage>();
			}

			if (type == MessageType.Error)
			{
				_success = false;
			}

			_messages.Add(new ResultMessage { Type = type, Message = message });
		}

        public void AddMessage(Exception e)
        {
			AddMessage(MessageType.Error, $"{e.Message} - {e.Source}");
        }

        public void Merge(Result from)
        {
			if (from?._messages?.Any() ?? false)
            {
				foreach (var m in from._messages)
				{
					AddMessage(m.Type, m.Message);
				}
			}
        }

		public bool Success => _success;
	}

	public class Result<T> : Result
	{
		public T Data { get; set; }
	}
}
