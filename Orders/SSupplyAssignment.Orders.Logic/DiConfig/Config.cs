﻿using System;
using Unity;

namespace SSupplyAssignment.Orders.Logic.DiConfig
{
    using SendGrid;
    using SSupplyAssignment.Common.Shared.Settings;
    using SSupplyAssignment.Order.Shared.Interface;
    using SSupplyAssignment.Orders.Logic.Managers;
    using SSupplyAssignment.Orders.Logic.Services;
    using SSupplyAssignment.Orders.Shared.Managers;
    using Unity.Injection;

    public static class Config
    {
        public static IUnityContainer ConfigureOrdersLogic(this IUnityContainer container)
        {
            container.RegisterType<IOrderManager, OrderManager>();
            container.RegisterType<IOrderQueryManager, OrderQueryManager>();
            return container;
        }

        public static IUnityContainer ConfigureEmailServices(this IUnityContainer container, EmailServiceSettings settings)
        {
            container.RegisterType<IEmailService, SendgridEmailService>();
            container.RegisterType<ISendGridClient, SendGridClient>(new InjectionConstructor(new SendGridClientOptions { ApiKey = settings.ApiKey }));
            return container;
        }
    }
}
