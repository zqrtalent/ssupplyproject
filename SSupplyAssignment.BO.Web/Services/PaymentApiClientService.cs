﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SSupplyAssignment.Payment.Api.Models.Request;

namespace SSupplyAssignment.BO.Web.Services
{
    internal class PaymentApiClientService
    {
        public readonly string _endpoint;
        public readonly string _userName;
        public readonly string _password;

        public PaymentApiClientService(string endpoint, string userName, string password)
        {
            _endpoint = endpoint;
            _userName = userName;
            _password = password;
        }

        public async Task CreateSuccessfulPaymentAsync(CreatePaymentApiModel model)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

                client.BaseAddress = new Uri(_endpoint);
                var response = await client.PostAsync($"api/payment/succeed", content);
            }

            await Task.CompletedTask;
        }

        public async Task CreateFailedPaymentAsync(CreatePaymentApiModel model)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

                client.BaseAddress = new Uri(_endpoint);
                var response = await client.PostAsync($"api/payment/failed", content);
            }

            await Task.CompletedTask;
        }


    }
}
