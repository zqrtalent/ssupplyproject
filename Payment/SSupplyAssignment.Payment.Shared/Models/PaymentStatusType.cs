﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SSupplyAssignment.Payment.Shared.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PaymentStatusType
    {
        Pending,

        Succeed,

        Failed
    }
}
