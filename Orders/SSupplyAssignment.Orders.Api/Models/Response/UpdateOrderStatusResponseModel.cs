﻿using System;
namespace SSupplyAssignment.Orders.Api.Models.Response
{
    public class UpdateOrderStatusResponseModel
    {
        public bool Success { get; set; }
    }
}
