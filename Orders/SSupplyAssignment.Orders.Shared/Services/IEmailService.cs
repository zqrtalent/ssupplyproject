﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Order.Shared.Interface
{
    public interface IEmailService
    {
        Task<Result> SendOrderIsReadyAsync(OrderModel order);
    }
}