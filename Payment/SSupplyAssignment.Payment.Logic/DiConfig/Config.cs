﻿using System;
using Unity;

namespace SSupplyAssignment.Payment.Logic.DiConfig
{
    using SSupplyAssignment.Payment.Logic.Managers;
    using SSupplyAssignment.Payment.Shared.Managers;

    public static class Config
    {
        public static IUnityContainer ConfigurePaymentLogic(this IUnityContainer container)
        {
            container.RegisterType<IPaymentManager, PaymentManager>();
            return container;
        }
    }
}
