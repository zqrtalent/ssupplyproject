﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SSupplyAssignment.Orders.Dal.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CustomerId = table.Column<long>(nullable: false),
                    CustomerEmail = table.Column<string>(maxLength: 50, nullable: true),
                    ServiceId = table.Column<long>(nullable: false),
                    ServiceAttributesJson = table.Column<string>(maxLength: 1024, nullable: true),
                    StatusTypeCode = table.Column<string>(maxLength: 50, nullable: true),
                    DateLastUpdatedUTC = table.Column<DateTime>(nullable: false),
                    DateCreatedUTC = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Orders");
        }
    }
}
