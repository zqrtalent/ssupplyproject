﻿using System;
namespace SSupplyAssignment.Payment.Shared.Models
{
    public class PaymentModel
    {
        public long Id { get; set; }

        public string PaymentKey { get; set; }

        public decimal Amount { get; set; }

        public string ProductItemCode { get; set; }

        public string StatusTypeCode
        {
            get
            {
                return StatusType.ToString();
            }
            set
            {
                var statusType = default(PaymentStatusType);
                Enum.TryParse(value, out statusType);
                StatusType = statusType;
            }
        }

        public PaymentStatusType StatusType { get; set; } = PaymentStatusType.Pending;

        public DateTime DateLastUpdatedUTC { get; set; } = DateTime.UtcNow;

        public DateTime DateCreatedUTC { get; set; } = DateTime.UtcNow;
    }
}
