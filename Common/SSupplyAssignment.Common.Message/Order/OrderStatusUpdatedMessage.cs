﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Common.Message.Order
{
    public class OrderStatusUpdatedMessage : EventMessage
    {
        public long OrderId { get; set; }

        public string CustomerEmail { get; set; }

        public string StatusTypeCodeOld { get; set; }

        public string StatusTypeCodeNew { get; set; }
    }
}
