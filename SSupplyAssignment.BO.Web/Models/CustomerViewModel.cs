using System;

namespace SSupplyAssignment.BO.Web.Models
{
    public class CustomerViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
