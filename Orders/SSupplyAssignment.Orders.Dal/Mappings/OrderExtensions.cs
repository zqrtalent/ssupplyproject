﻿using System;
using SSupplyAssignment.Orders.Shared.Models;
using Entities = SSupplyAssignment.Orders.Dal.Entities;

namespace SSupplyAssignment.Orders.Dal.Mappings
{
    public static class OrderExtensions
    {
        public static OrderModel MapToModel(this Entities.Order entity)
        {
            if (entity == null) return null;
            return new OrderModel
            {
                Id = entity.Id,
                StatusTypeCode = entity.StatusTypeCode,
                CustomerEmail = entity.CustomerEmail,
                CustomerId = entity.CustomerId,
                DateCreatedUTC = entity.DateCreatedUTC,
                DateLastUpdatedUTC = entity.DateLastUpdatedUTC,
                ServiceAttributesJson = entity.ServiceAttributesJson,
                ServiceId = entity.ServiceId,
            };
        }

        public static Entities.Order MapToEntity(this OrderModel model, bool includeId = true)
        {
            if (model == null) return null;
            return new Entities.Order
            {
                Id = includeId ? model.Id : 0,
                StatusTypeCode = model.StatusTypeCode,
                CustomerEmail = model.CustomerEmail,
                CustomerId = model.CustomerId,
                DateCreatedUTC = model.DateCreatedUTC,
                DateLastUpdatedUTC = model.DateLastUpdatedUTC,
                ServiceAttributesJson = model.ServiceAttributesJson,
                ServiceId = model.ServiceId,
            };
        }
    }
}
