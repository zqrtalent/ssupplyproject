using System;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.BO.Web.Models
{
    public class CreateOrderViewModel
    {
        [Display(Name = "CustomerId")]
        public long CustomerId { get; set; }

        [Display(Name = "CustomerEmail")]
        public string CustomerEmail { get; set; }

        [Display(Name = "ServiceId")]
        public long ServiceId { get; set; }

        [Display(Name = "Service notes")]
        public string ServiceAttributes { get; set; }
    }
}