﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Order.Shared.Interface
{
    public interface IOrderDalService
    {
        /// <summary>
        /// Create order entry.
        /// </summary>
        /// <param name="order"></param>
        /// <returns>Order model object.</returns>
        Task<Result<OrderModel>> CreateAsync(OrderModel order);

        /// <summary>
        /// Updates order status.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<Result<Tuple<OrderModel,int>>> UpdateOrderStatusAsync(long id, OrderStatusType status);
    }
}
