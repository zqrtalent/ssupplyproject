﻿using Microsoft.AspNetCore.Builder;

namespace SSupplyAssignment.Orders.Api.Extensions
{
    using SSupplyAssignment.Common.Message.Order;
    using SSupplyAssignment.Common.Message.Payment;
    using SSupplyAssignment.Common.Shared;
    using SSupplyAssignment.EventBus.Shared;
    using SSupplyAssignment.Orders.Api.Handlers;
    using Unity;

    public static class ApplicationBuilderExtensions
    {
        public static void UseEventBus(this IApplicationBuilder app)
        {
            var eventBus = Program._container.Resolve<IEventBus>();
            eventBus.Subscribe<CreateOrderMessage, CreateOrderMessageHandler>();
            eventBus.Subscribe<UpdateOrderStatusMessage, UpdateOrderStatusMessageHandler>();
            eventBus.Subscribe<OrderStatusUpdatedMessage, OrderStatusUpdatedMessageHandler>();
            //eventBus.Subscribe<PaymentSucceededMessage, PaymentSucceededMessageHandler>();

            var eventBusPayment = Program._container.Resolve<IEventBus>(EventBusConstants.PaymentBusInstanceName);
            eventBusPayment.Subscribe<PaymentSucceededMessage, PaymentSucceededMessageHandler>();
        }
    }
}
