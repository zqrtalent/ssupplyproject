﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.EventBus.Shared;
using SSupplyAssignment.Payment.Api.Models.Message;
using SSupplyAssignment.Payment.Api.Models.Request;
using SSupplyAssignment.Payment.Shared.Models;
using Unity;

namespace SSupplyAssignment.Payment.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IUnityContainer _container;
        private readonly IEventBus _eventBus;
        private readonly ILogger<PaymentController> _logger;

        public PaymentController(IUnityContainer container, ILogger<PaymentController> logger)
        {
            _container = container;
            _eventBus = container.Resolve<IEventBus>();
            _logger = logger;
        }

        [HttpPost()]
        [Route("succeed")]
        public async Task<IActionResult> CreateSuccessful([FromBody]CreatePaymentRequestModel model)
        {
            var result = await ExecuteApiAsync(nameof(CreateSuccessful), async () =>
            {
                _eventBus.Publish(new CreatePaymentMessage
                {
                    Amount = model.Amount,
                    PaymentKey = model.PaymentKey,
                    ProductItemCode = model.ProductItemCode,
                    PaymentStatusTypeCode = PaymentStatusType.Succeed.ToString()
                });

                _logger.LogInformation($"{nameof(CreatePaymentMessage)} command sent...");

                await Task.CompletedTask;

                return new Result<object>();
            }, model);

            if (result.Success)
            {
                return Accepted(); // Need to wait for the processing of the command.
            }
            return BadRequest();
        }

        [HttpPost()]
        [Route("failed")]
        public async Task<IActionResult> CreateFailed([FromBody]CreatePaymentRequestModel model)
        {
            var result = await ExecuteApiAsync(nameof(CreateFailed), async () =>
            {
                _eventBus.Publish(new CreatePaymentMessage
                {
                    Amount = model.Amount,
                    PaymentKey = model.PaymentKey,
                    ProductItemCode = model.ProductItemCode,
                    PaymentStatusTypeCode = PaymentStatusType.Failed.ToString()
                });

                _logger.LogInformation($"{nameof(CreatePaymentMessage)} command sent...");

                await Task.CompletedTask;

                return new Result<object>();
            }, model);

            if (result.Success)
            {
                return Accepted(); // Need to wait for the processing of the command.
            }
            return BadRequest();
        }

        private async Task<Result<T>> ExecuteApiAsync<T>(string actionName, Func<Task<Result<T>>> funcExecute, object request)
        {
            Result<T> result = null;
            var sw = new Stopwatch();
            sw.Start();

            try
            {
                result = await funcExecute();
            }
            catch(Exception e)
            {
                if (result == null)
                    result = new Result<T>();
                result.AddMessage(e);
            }
            finally
            {
                sw.Stop();

                _logger.LogInformation("{Controller}->{Action}: {Duration}ms {RequestJson} {ResponseJson}",
                    this.GetType().Name,
                    actionName,
                    sw.ElapsedMilliseconds,
                    JsonConvert.SerializeObject(request),
                    JsonConvert.SerializeObject(result));
            }

            return result;
        }
    }
}
