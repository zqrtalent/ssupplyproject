﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SSupplyAssignment.Common.Message.Order;
using SSupplyAssignment.EventBus.Shared;

namespace SSupplyAssignment.Orders.Api.Handlers
{
    using System.Linq;
    using Serilog.Context;
    using SSupplyAssignment.Core.Shared.Models;
    using SSupplyAssignment.Order.Shared.Interface;
    using SSupplyAssignment.Orders.Shared.Models;

    public class OrderStatusUpdatedMessageHandler : IEventMessageHandler<OrderStatusUpdatedMessage>
    {
        private readonly IEmailService _emailService;
        private readonly ILogger<OrderStatusUpdatedMessageHandler> _logger;

        public OrderStatusUpdatedMessageHandler(IEmailService emailService, ILogger<OrderStatusUpdatedMessageHandler> logger)
        {
            _emailService = emailService;
            _logger = logger;
        }

        public async Task Handle(OrderStatusUpdatedMessage message)
        {
            var result = new Result();
            if (message.StatusTypeCodeNew == OrderStatusType.Ready.ToString())
            {
                var sendResult = await _emailService.SendOrderIsReadyAsync(new OrderModel
                {
                    Id = message.OrderId,
                    StatusTypeCode = message.StatusTypeCodeNew,
                    CustomerEmail = message.CustomerEmail
                });
                result.Merge(sendResult);
            }
            _logger.LogInformation("OrderStatusUpdatedMessageHandler->Handle: {messageId} {success}", message.Id, result.Success);
        }
    }
}
