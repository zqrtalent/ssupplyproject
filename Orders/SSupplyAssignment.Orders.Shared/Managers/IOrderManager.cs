﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Shared.Managers
{
    public interface IOrderManager
    {
        /// <summary>
        /// Creates new order.
        /// </summary>
        /// <param name="order">Order model</param>
        /// <returns></returns>
        Task<Result<OrderModel>> CreateOrderAsync(OrderModel order);

        /// <summary>
        /// Updates status of the order
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="statusNew">Order status type</param>
        /// <returns></returns>
        Task<Result<int>> UpdateOrderStatusAsync(long id, OrderStatusType statusNew);
    }
}
