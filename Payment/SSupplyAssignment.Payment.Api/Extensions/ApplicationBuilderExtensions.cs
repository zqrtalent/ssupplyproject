﻿using Microsoft.AspNetCore.Builder;

namespace SSupplyAssignment.Payment.Api.Extensions
{
    using SSupplyAssignment.EventBus.Shared;
    using SSupplyAssignment.Payment.Api.Handlers;
    using SSupplyAssignment.Payment.Api.Models.Message;
    using Unity;

    public static class ApplicationBuilderExtensions
    {
        public static void UseEventBus(this IApplicationBuilder app)
        {
            var eventBus = Program._container.Resolve<IEventBus>();
            eventBus.Subscribe<CreatePaymentMessage, CreatePaymentMessageHandler>();
        }
    }
}
