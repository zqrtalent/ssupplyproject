using System;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.BO.Web.Models
{
    public class MarkAsReadyViewModel
    {
        public long OrderId { get; set; }

        public string Notes { get; set; }
    }
}
