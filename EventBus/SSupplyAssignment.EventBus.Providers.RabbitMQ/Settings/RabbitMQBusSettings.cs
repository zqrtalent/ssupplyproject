﻿using System;
namespace SSupplyAssignment.EventBus.Providers.RabbitMQ.Settings
{
    public class RabbitMQBusSettings
    {
        public string BusConnectionString { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public int RetryCt { get; set; }

        public string SubscriptionQueueName { get; set; }
    }
}
