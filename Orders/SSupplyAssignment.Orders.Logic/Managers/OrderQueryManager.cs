﻿using System;
using SSupplyAssignment.Orders.Shared.Managers;

namespace SSupplyAssignment.Orders.Logic.Managers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSupplyAssignment.Core.Shared.Models;
    using SSupplyAssignment.Order.Shared.Interface;
    using SSupplyAssignment.Orders.Shared.Models;

    internal class OrderQueryManager : IOrderQueryManager
    {
        private readonly IOrderReadOnlyDalService _orderReadonlyDalService;

        public OrderQueryManager(IOrderReadOnlyDalService orderReadonlyDalService)
        {
            _orderReadonlyDalService = orderReadonlyDalService;
        }

        public async Task<Result<OrderModel>> GetOrderByIdAsync(long id)
        {
            if (id <= 0)
            {
                throw new ArgumentException($"{nameof(id)} is a negative or zero value!");
            }
            return await _orderReadonlyDalService.GetByIdAsync(id);
        }

        public async Task<Result<IReadOnlyCollection<OrderModel>>> GetOrdersByStatusAsync(OrderStatusType status, int offset = 0, int pageSize = 100)
        {
            return await _orderReadonlyDalService.GetByStatusAsync(status, offset, pageSize);
        }
    }
}
