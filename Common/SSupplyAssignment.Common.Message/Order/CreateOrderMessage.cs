﻿using System;
using System.Collections.Generic;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Common.Message.Order
{
    public class CreateOrderMessage : EventMessage
    {
        public long CustomerId { get; set; }

        public string CustomerEmail { get; set; }

        public long ServiceId { get; set; }

        public Dictionary<string, string> ServiceAttributes { get; set; }
    }
}
