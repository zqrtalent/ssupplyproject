﻿using System;
using SSupplyAssignment.Payment.Shared.Models;

namespace SSupplyAssignment.Payment.Api.Models
{
    public class PaymentApiModel
    {
        public long Id { get; set; }

        public string PaymentKey { get; set; }

        public string ProductItemCode { get; set; }

        public decimal Amount { get; set; }

        public PaymentStatusType StatusType { get; set; }

        public DateTime DateLastUpdatedUTC { get; set; }

        public DateTime DateCreatedUTC { get; set; }
    }
}
