using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Unity;
using Unity.Microsoft.DependencyInjection;

namespace SSupplyAssignment.Orders.Api
{
    using Serilog.Formatting.Compact;
    using SSupplyAssignment.Orders.Api.Extensions;
    using SSupplyAssignment.Orders.Dal.Context;

    public class Program
    {
        public const string _appName  = "OrdersApi";
        public const string _mainSettingsSection = "OrderApi";
        public static readonly IUnityContainer  _container  = new UnityContainer();

        public static int Main(string[] args)
        {
            var configuration = BuildConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);

            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", _appName);
                var host = CreateWebHost(args, configuration);

                Log.Information("Applying migrations ({ApplicationContext})...", _appName);
                host.MigrateDbContext<OrdersDbContext>((context, services) =>
                {
                }, retryExponentially: false);

                Log.Information("Starting web host ({ApplicationContext})...", _appName);

                host.Run();

                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", _appName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHost CreateWebHost(string[] args, IConfiguration configuration)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(configuration)
                .UseSerilog()
                .UseUnityServiceProvider(_container)
                .UseStartup<Startup>()
                //.UseApplicationInsights()
                .Build();
        }

        private static IConfiguration BuildConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        private static Serilog.ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            var seqServerUrl = configuration["Serilog:SeqServerUrl"];
            return new LoggerConfiguration()
                .Enrich.WithProperty("ApplicationContext", _appName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                //.WriteTo.File(new RenderedCompactJsonFormatter(), "log.json")
                .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
                //.WriteTo.Http(string.IsNullOrWhiteSpace(logstashUrl) ? "http://logstash:8080" : logstashUrl)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
    }
}
