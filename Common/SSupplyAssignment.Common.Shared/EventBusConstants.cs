﻿using System;
namespace SSupplyAssignment.Common.Shared
{
    public static class EventBusConstants
    {
        public const string OrderBusInstanceName = "OrderBus";
        public const string PaymentBusInstanceName = "PaymentBus";
    }
}
