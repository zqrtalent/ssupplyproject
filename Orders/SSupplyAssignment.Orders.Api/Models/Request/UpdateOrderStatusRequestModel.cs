﻿using System;
using System.ComponentModel.DataAnnotations;
using SSupplyAssignment.Orders.Shared.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SSupplyAssignment.Orders.Api.Models.Request
{
    public class UpdateOrderStatusRequestModel
    {
        [Required]
        public long OrderId { get; set; }

        [Required]
        [JsonProperty(), JsonConverter(typeof(StringEnumConverter))]
        public OrderStatusType StatusNew { get; set; }
    }
}
