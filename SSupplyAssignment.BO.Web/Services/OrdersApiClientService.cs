﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SSupplyAssignment.BO.Web.Models;
using SSupplyAssignment.Orders.Api.Models.Request;

namespace SSupplyAssignment.BO.Web.Services
{
    internal class OrdersApiClientService
    {
        public readonly string _endpoint;
        public readonly string _userName;
        public readonly string _password;

        public OrdersApiClientService(string endpoint, string userName, string password)
        {
            _endpoint = endpoint;
            _userName = userName;
            _password = password;
        }

        public async Task CreateOrderAsync(CreateOrderApiModel model)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

                client.BaseAddress = new Uri(_endpoint);
                var response = await client.PostAsync($"api/order/create", content);
            }

            await Task.CompletedTask;
        }

        public async Task UpdateOrderStatusAsync(UpdateOrderStatusApiModel model)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

                client.BaseAddress = new Uri(_endpoint);
                var response = await client.PutAsync($"api/order/status", content);
            }

            await Task.CompletedTask;
        }

        public async Task<IEnumerable<OrderViewModel>> GetOrdersByStatus(string orderStatusCode)
        {
            var result = Enumerable.Empty<OrderViewModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_endpoint);
                var response = await client.GetAsync($"api/order/{orderStatusCode}/0/100");

                if(response.IsSuccessStatusCode)
                {
                    var responeOrders = JsonConvert.DeserializeObject<List<OrderApiModel>>((await response.Content.ReadAsStringAsync()));
                    result = responeOrders?.Select(x => new OrderViewModel
                    {
                        Id = x.Id,
                        CreatedUtc = x.DateCreatedUTC,
                        CustomerEmail = x.CustomerEmail,
                        CustomerId = x.CustomerId,
                        LastUpdatedUtc = x.DateLastUpdatedUTC,
                        ServiceAttributes = x.ServiceAttributesJson,
                        ServiceId = x.ServiceId,
                        StatusTypeCode = x.StatusType

                    })?.ToList();
                }
            }

            return result;
        }

        public async Task<OrderViewModel> GetOrderById(long id)
        {
            OrderViewModel result = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_endpoint);
                var response = await client.GetAsync($"api/order/{id}");

                if (response.IsSuccessStatusCode)
                {
                    var responeOrder = JsonConvert.DeserializeObject<OrderApiModel>((await response.Content.ReadAsStringAsync()));
                    result = new OrderViewModel
                    {
                        Id = responeOrder.Id,
                        CreatedUtc = responeOrder.DateCreatedUTC,
                        CustomerEmail = responeOrder.CustomerEmail,
                        CustomerId = responeOrder.CustomerId,
                        LastUpdatedUtc = responeOrder.DateLastUpdatedUTC,
                        ServiceAttributes = responeOrder.ServiceAttributesJson,
                        ServiceId = responeOrder.ServiceId,
                        StatusTypeCode = responeOrder.StatusType
                    };
                }
            }

            return result;
        }
    }
}
