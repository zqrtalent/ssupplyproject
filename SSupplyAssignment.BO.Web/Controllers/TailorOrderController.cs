﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SSupplyAssignment.BO.Web.Models;
using SSupplyAssignment.BO.Web.Services;
using SSupplyAssignment.BO.Web.Settings;
using SSupplyAssignment.Orders.Api.Models.Request;

namespace SSupplyAssignment.BO.Web.Controllers
{
    public class TailorOrderController : Controller
    {
        private readonly ILogger<TailorOrderController> _logger;

        private readonly OrdersApiClientService _apiClient;

        public TailorOrderController(ILogger<TailorOrderController> logger, IOptions<BoWebSettings> options)
        {
            _logger = logger;
            var settings = options.Value;
            _apiClient = new OrdersApiClientService(settings.OrderApi.Endpoint, settings.OrderApi.Username, settings.OrderApi.Password);
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var orders = await _apiClient.GetOrdersByStatus("Paid");

            //var orders = new List<OrderViewModel>
            //{
            //    new OrderViewModel
            //    {
            //        Id = 1,
            //        CreatedUtc = DateTime.UtcNow,
            //        LastUpdatedUtc = DateTime.UtcNow,
            //        CustomerEmail = "zzz@gmail.com",
            //        CustomerName = "zzz",
            //        ServiceAttributes = "left=-2cm,right-2.5cm",
            //        ServiceName = "Sleeve alteration"
            //    },
            //    new OrderViewModel
            //    {
            //        Id = 2,
            //        CreatedUtc = DateTime.UtcNow,
            //        LastUpdatedUtc = DateTime.UtcNow,
            //        CustomerEmail = "zzz@gmail.com",
            //        CustomerName = "zzz",
            //        ServiceAttributes = "left=-2cm,right-2.5cm",
            //        ServiceName = "Sleeve alteration"
            //    },
            //    new OrderViewModel
            //    {
            //        Id = 3,
            //        CreatedUtc = DateTime.UtcNow,
            //        LastUpdatedUtc = DateTime.UtcNow,
            //        CustomerEmail = "zzz@gmail.com",
            //        CustomerName = "zzz",
            //        ServiceAttributes = "left=-2cm,right-2.5cm",
            //        ServiceName = "Sleeve alteration"
            //    }
            //};

            return View("TailorOrders", orders ?? Enumerable.Empty<OrderViewModel>());
        }

        [HttpGet]
        public IActionResult MarkAsReady(long id)
        {
            var model = new MarkAsReadyViewModel
            {
                OrderId = id,
                Notes = "The order is ready!"
            };

            return View("TailorUpdateOrder", model);
        }

        [HttpPost]
        public async Task<IActionResult> MarkAsReady(MarkAsReadyViewModel model)
        {
            await _apiClient.UpdateOrderStatusAsync(new UpdateOrderStatusApiModel
            {
                OrderId = model.OrderId,
                StatusNew = "Ready"
            });
            return RedirectToAction("Index");
        }
    }
}
