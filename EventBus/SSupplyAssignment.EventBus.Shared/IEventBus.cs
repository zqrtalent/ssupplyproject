﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.EventBus.Shared
{
    public interface IEventBus : IDisposable
    {
        void Publish(EventMessage message);

        void Subscribe<T, TH>()
            where T : EventMessage
            where TH : IEventMessageHandler<T>;

        void SubscribeDynamic<TH>(string eventName)
            where TH : IDynamicEventMessageHandler;

        void UnsubscribeDynamic<TH>(string eventName)
            where TH : IDynamicEventMessageHandler;

        void Unsubscribe<T, TH>()
            where TH : IEventMessageHandler<T>
            where T : EventMessage;
    }
}
