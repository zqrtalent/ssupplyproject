﻿using System;

namespace SSupplyAssignment.Common.Logic
{
    public class EventBusReceiver
    {
        public string Name { get; set; }

        public string TopicName { get; set; }
    }

    public static class EventBusRegistration
    {
        public static void RegisterMessageReceiver()
        {

        }
    }
}
