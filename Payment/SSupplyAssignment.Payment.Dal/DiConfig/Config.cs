﻿using Unity;
using SSupplyAssignment.Core.Shared.Settings;

namespace SSupplyAssignment.Payment.Dal.DiConfig
{
    using Unity.Injection;
    using Microsoft.EntityFrameworkCore;
    using SSupplyAssignment.Payment.Dal.Context;
    using SSupplyAssignment.Payment.Shared.Interface;
    using SSupplyAssignment.Payment.Dal.Services;

    public static class Config
    {
        public static IUnityContainer ConfigurePaymentDal(this IUnityContainer container, DbConnectionSettings settings)
        {
            // Add DbContext
            var optsBuilder = new DbContextOptionsBuilder<PaymentDbContext>()
                .UseMySql(settings.ConnectionString, optionsBuilder => 
                { 
                    optionsBuilder.CommandTimeout(settings.CommandTimeout); 
                    // MySql throws InvalidOperation and thats the reason we cant use Polly for retry policy.
                    if(settings.ExponentialRetry)
                    {
                        optionsBuilder.EnableRetryOnFailure(10);
                    }
                });
            container.RegisterType<IPaymentDbContext, PaymentDbContext>(new InjectionConstructor(optsBuilder.Options, settings.Schema));
            container.RegisterType<PaymentDbContext>(new InjectionConstructor(optsBuilder.Options, settings.Schema));

            container.RegisterType<IPaymentDalService, PaymentDalService>();
            return container;
        }
    }
}

