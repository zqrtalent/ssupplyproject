﻿using Unity;
using SSupplyAssignment.Core.Shared.Settings;

namespace SSupplyAssignment.Orders.Dal.DiConfig
{
    using Unity.Injection;
    using Microsoft.EntityFrameworkCore;
    using SSupplyAssignment.Orders.Dal.Context;
    using SSupplyAssignment.Order.Shared.Interface;
    using SSupplyAssignment.Orders.Dal.Services;

    public static class Config
    {
        public static IUnityContainer ConfigureOrdersDal(this IUnityContainer container, DbConnectionSettings settings)
        {
            // Add DbContext
            var optsBuilder = new DbContextOptionsBuilder<OrdersDbContext>()
                .UseMySql(settings.ConnectionString, optionsBuilder => 
                { 
                    optionsBuilder.CommandTimeout(settings.CommandTimeout); 
                    // MySql throws InvalidOperation and thats the reason we cant use Polly for retry policy.
                    if(settings.ExponentialRetry)
                    {
                        optionsBuilder.EnableRetryOnFailure(10);
                    }
                });
            container.RegisterType<IOrdersDbContext, OrdersDbContext>(new InjectionConstructor(optsBuilder.Options, settings.Schema));
            container.RegisterType<OrdersDbContext>(new InjectionConstructor(optsBuilder.Options, settings.Schema));

            container.RegisterType<IOrderDalService, OrderDalService>();
            container.RegisterType<IOrderReadOnlyDalService, OrderReadOnlyDalService>();
            return container;
        }
    }
}

