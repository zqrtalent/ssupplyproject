﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SSupplyAssignment.Common.Message.Order;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.EventBus.Shared;
using SSupplyAssignment.Orders.Api.Extensions;
using SSupplyAssignment.Orders.Api.Models.Request;
using SSupplyAssignment.Orders.Api.Models.Response;
using SSupplyAssignment.Orders.Shared.Managers;
using SSupplyAssignment.Orders.Shared.Models;
using Unity;

namespace SSupplyAssignment.Orders.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IUnityContainer _container;
        private readonly IEventBus _eventBus;
        private readonly IOrderQueryManager _orderQueryManager;
        private readonly ILogger<OrderController> _logger;

        public OrderController(IUnityContainer container, ILogger<OrderController> logger)
        {
            _container = container;
            _eventBus = container.Resolve<IEventBus>();
            _orderQueryManager = container.Resolve<IOrderQueryManager>();
            _logger = logger;
        }

        [HttpGet()]
        [Route("{id}")]
        public async Task<IActionResult> Get([FromRoute]long id)
        {
            var result = await ExecuteApiAsync(nameof(Get), async () => await _orderQueryManager.GetOrderByIdAsync(id), new { OrderId = id });
            if(result.Success && result.Data != null)
            {
                return Ok(result.Data.ToApiModel());
            }
            return NotFound();
        }

        [HttpGet()]
        [Route("{status}/{ofset}/{page}")]
        public async Task<IActionResult> GetByStatus([FromRoute]OrderStatusType status, [FromRoute]int offset, [FromRoute]int page)
        {
            var result = await ExecuteApiAsync(nameof(GetByStatus),
                    async () => await _orderQueryManager.GetOrdersByStatusAsync(status),
                    new { Status = status, Offset = offset, Page = page });
            if (result.Success && result.Data != null)
            {
                return Ok(result.Data.ToApiModel());
            }
            return NotFound();
        }

        [HttpPost()]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody]CreateOrderRequestModel model)
        {
            var result = await ExecuteApiAsync(nameof(Create), async () =>
            {
                _eventBus.Publish(new CreateOrderMessage
                {
                    CustomerId = model.CustomerId,
                    CustomerEmail = model.CustomerEmail,
                    ServiceAttributes = model.ServiceAttributes,
                    ServiceId = model.ServiceId,
                });

                _logger.LogInformation($"{nameof(CreateOrderMessage)} command sent...");

                await Task.CompletedTask;

                return new Result<object>();
            }, model);

            if (result.Success)
            {
                return Accepted(); // Need to wait for the processing of the command.
            }
            return BadRequest();
        }

        [HttpPut()]
        [Route("status")]
        public async Task<IActionResult> UpdateStatus([FromBody]UpdateOrderStatusRequestModel model)
        {
            var result = await ExecuteApiAsync(nameof(Create), async () =>
            {
                _eventBus.Publish(new UpdateOrderStatusMessage
                {
                    OrderId = model.OrderId,
                    StatusTypeCode = model.StatusNew.ToString(),
                });

                _logger.LogInformation($"{nameof(CreateOrderMessage)} command sent...");

                await Task.CompletedTask;

                return new Result<object>();
            }, model);

            if (result.Success)
            {
                return Accepted(); // Need to wait for the processing of the command.
            }
            return BadRequest();
        }

        private async Task<Result<T>> ExecuteApiAsync<T>(string actionName, Func<Task<Result<T>>> funcExecute, object request)
        {
            Result<T> result = null;
            var sw = new Stopwatch();
            sw.Start();

            try
            {
                result = await funcExecute();
            }
            catch(Exception e)
            {
                if (result == null)
                    result = new Result<T>();
                result.AddMessage(e);
            }
            finally
            {
                sw.Stop();

                _logger.LogInformation("{Controller}->{Action}: {Duration}ms {RequestJson} {ResponseJson}",
                    this.GetType().Name,
                    actionName,
                    sw.ElapsedMilliseconds,
                    JsonConvert.SerializeObject(request),
                    JsonConvert.SerializeObject(result));
            }

            return result;
        }
    }
}
