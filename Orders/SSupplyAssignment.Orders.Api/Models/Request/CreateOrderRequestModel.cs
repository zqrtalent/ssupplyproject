﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Api.Models.Request
{
    public class CreateOrderRequestModel
    {
        [Required]
        public long CustomerId { get; set; }

        [Required]
        public string CustomerEmail { get; set; }

        [Required]
        public long ServiceId { get; set; }

        [Required]
        public Dictionary<string, string> ServiceAttributes { get; set; }
    }
}
