﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SSupplyAssignment.Common.Message.Order;
using SSupplyAssignment.EventBus.Shared;
using SSupplyAssignment.Payment.Shared.Managers;

namespace SSupplyAssignment.Payment.Api.Handlers
{
    using System.Linq;
    using Serilog.Context;
    using SSupplyAssignment.Payment.Api.Models.Message;
    using SSupplyAssignment.Payment.Shared.Models;

    public class CreatePaymentMessageHandler: IEventMessageHandler<CreatePaymentMessage>
    {
        private readonly IPaymentManager _manager;
        private readonly ILogger<CreatePaymentMessageHandler> _logger;

        public CreatePaymentMessageHandler(IPaymentManager manager, ILogger<CreatePaymentMessageHandler> logger)
        {
            _manager = manager;
            _logger = logger;
        }

        public async Task Handle(CreatePaymentMessage message)
        {
            var createResult = await _manager.CreatePaymentAsync(new PaymentModel
            {
                PaymentKey = message.PaymentKey,
                Amount = message.Amount,
                ProductItemCode = message.ProductItemCode,
                StatusTypeCode = message.PaymentStatusTypeCode,
            });

            _logger.LogInformation("CreatePaymentMessageHandler->Handle: {messageId} {success}", message.Id, createResult.Success);
        }
    }
}
