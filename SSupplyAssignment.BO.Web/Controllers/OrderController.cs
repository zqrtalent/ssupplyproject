﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SSupplyAssignment.BO.Web.Models;
using SSupplyAssignment.BO.Web.Services;
using SSupplyAssignment.BO.Web.Settings;
using SSupplyAssignment.Orders.Api.Models.Request;
using SSupplyAssignment.Payment.Api.Models.Request;

namespace SSupplyAssignment.BO.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly ILogger<OrderController> _logger;
        private readonly OrdersApiClientService _apiClient;
        private readonly PaymentApiClientService _paymentApiClient;

        public OrderController(ILogger<OrderController> logger, IOptions<BoWebSettings> options)
        {
            _logger = logger;

            var settings = options.Value;
            _apiClient = new OrdersApiClientService(settings.OrderApi.Endpoint,settings.OrderApi.Username, settings.OrderApi.Password);
            _paymentApiClient = new PaymentApiClientService(settings.PaymentApi.Endpoint, settings.PaymentApi.Username, settings.PaymentApi.Password);
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var orders = await _apiClient.GetOrdersByStatus("Created");
            //var orders = new List<OrderViewModel>
            //{
            //    new OrderViewModel
            //    {
            //        Id = 1,
            //        CreatedUtc = DateTime.UtcNow,
            //        LastUpdatedUtc = DateTime.UtcNow,
            //        CustomerEmail = "zzz@gmail.com",
            //        CustomerName = "zzz",
            //        ServiceAttributes = "left=-2cm,right-2.5cm",
            //        ServiceName = "Sleeve alteration",
            //        StatusTypeCode = "Created",
            //    },
            //    new OrderViewModel
            //    {
            //        Id = 2,
            //        CreatedUtc = DateTime.UtcNow,
            //        LastUpdatedUtc = DateTime.UtcNow,
            //        CustomerEmail = "zzz@gmail.com",
            //        CustomerName = "zzz",
            //        ServiceAttributes = "left=-2cm,right-2.5cm",
            //        ServiceName = "Sleeve alteration",
            //        StatusTypeCode = "Created",
            //    },
            //    new OrderViewModel
            //    {
            //        Id = 3,
            //        CreatedUtc = DateTime.UtcNow,
            //        LastUpdatedUtc = DateTime.UtcNow,
            //        CustomerEmail = "zzz@gmail.com",
            //        CustomerName = "zzz",
            //        ServiceAttributes = "left=-2cm,right-2.5cm",
            //        ServiceName = "Sleeve alteration",
            //        StatusTypeCode = "Created",
            //    }
            //};

            return View("Orders", orders ?? Enumerable.Empty<OrderViewModel>());
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateOrderViewModel
            {
            };

            return View("CreateOrder", model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(long id)
        {
            var model = await _apiClient.GetOrderById(id);
            //var model = new OrderViewModel
            //{
            //    Id = 3,
            //    CreatedUtc = DateTime.UtcNow,
            //    LastUpdatedUtc = DateTime.UtcNow,
            //    CustomerEmail = "zzz@gmail.com",
            //    CustomerName = "zzz",
            //    ServiceAttributes = "left=-2cm,right-2.5cm",
            //    ServiceName = "Sleeve alteration",
            //    StatusTypeCode = "Created"
            //};

            return View("EditOrder", model);
        }

        [HttpGet]
        public IActionResult Pay(long id)
        {
            var model = new PayViewModel
            {
               OrderId = id,
               Amount = 56.76m
            };

            return View("PayForOrder", model);
        }

        [HttpPost]
        public async Task<IActionResult> PayForOrder(PayViewModel model)
        {
            await _paymentApiClient.CreateSuccessfulPaymentAsync(new CreatePaymentApiModel
            {
                ProductItemCode = model.OrderId.ToString(),
                Amount = model.Amount,
                PaymentKey = $"pkey_{Guid.NewGuid().ToString("n")}"
            });

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult EditOrder(OrderViewModel model)
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewOrder(CreateOrderViewModel model)
        {
            await _apiClient.CreateOrderAsync(new CreateOrderApiModel
            {
                CustomerEmail = model.CustomerEmail,
                CustomerId = model.CustomerId,
                ServiceId = model.ServiceId,
                ServiceAttributes = new Dictionary<string, string> { { "Notes", model.ServiceAttributes } }
            });

            return RedirectToAction("Index");
        }
    }
}
