﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.Orders.Api.Models.Request
{
    public class OrderApiModel
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }

        public string CustomerEmail { get; set; }

        public long ServiceId { get; set; }

        public string ServiceAttributesJson { get; set; }

        public string StatusType { get; set; }

        public DateTime DateLastUpdatedUTC { get; set; } 

        public DateTime DateCreatedUTC { get; set; }
    }
}
