﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Shared.Managers
{
    public interface IOrderQueryManager
    {
        /// <summary>
        /// Retrieves single order by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Order model</returns>
        Task<Result<OrderModel>> GetOrderByIdAsync(long id);

        /// <summary>
        /// Retrieves list of orders by status
        /// </summary>
        /// <param name="status"></param>
        /// <returns>List of orders.</returns>
        Task<Result<IReadOnlyCollection<OrderModel>>> GetOrdersByStatusAsync(OrderStatusType status, int offset = 0, int pageSize = 100);
    }
}
