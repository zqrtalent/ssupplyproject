﻿using System;
using SSupplyAssignment.Orders.Shared.Managers;

namespace SSupplyAssignment.Orders.Logic.Managers
{
    using System.Threading.Tasks;
    using SSupplyAssignment.Common.Message.Order;
    using SSupplyAssignment.Core.Shared.Models;
    using SSupplyAssignment.EventBus.Shared;
    using SSupplyAssignment.Order.Shared.Interface;
    using SSupplyAssignment.Orders.Shared.Models;

    internal class OrderManager : IOrderManager
    {
        private readonly IOrderDalService _orderDalService;
        private readonly IOrderReadOnlyDalService _orderReadOnlyDalService;
        private readonly IEventBus _eventBus;

        public OrderManager(IOrderDalService orderDalService, IEventBus eventBus, IOrderReadOnlyDalService orderReadOnlyDalService)
        {
            _orderDalService = orderDalService;
            _orderReadOnlyDalService = orderReadOnlyDalService;
            _eventBus = eventBus;
        }

        public async Task<Result<OrderModel>> CreateOrderAsync(OrderModel order)
        {
            if(order == null)
            {
                throw new ArgumentNullException($"{nameof(order)}");
            }

            var result = new Result<OrderModel>();
            var createResult = await _orderDalService.CreateAsync(order);
            result.Merge(createResult);
            result.Data = createResult.Data;

            if(result.Success && result.Data != null)
            {
                _eventBus.Publish(new OrderCreatedMessage
                {
                    OrderId = result.Data.Id,
                    DateCreatedUtc = result.Data.DateCreatedUTC
                });
            }

            return result;
        }

        public async Task<Result<int>> UpdateOrderStatusAsync(long id, OrderStatusType statusNew)
        {
            if (id <= 0)
            {
                throw new ArgumentException($"{nameof(id)} is a negative or zero value!");
            }

            var updateResult = await _orderDalService.UpdateOrderStatusAsync(id, statusNew);
            if(updateResult.Success && updateResult.Data != null && updateResult.Data.Item2 == 1)
            {
                _eventBus.Publish(new OrderStatusUpdatedMessage
                {
                     OrderId = id,
                     CustomerEmail = updateResult.Data.Item1?.CustomerEmail ?? string.Empty,
                     StatusTypeCodeNew = statusNew.ToString(),
                     StatusTypeCodeOld = updateResult.Data.Item1?.StatusTypeCode ?? string.Empty
                });
            }

            var result = new Result<int>();
            result.Merge(updateResult);
            result.Data = updateResult.Data?.Item2 ?? 0;
            return result;
        }
    }
}
