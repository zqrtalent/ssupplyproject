﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.EventBus.Shared
{
    public interface IEventMessageHandler<in TEventMessage> : IEventMessageHandler where TEventMessage : EventMessage
    {
        Task Handle(TEventMessage message);
    }

    public interface IEventMessageHandler
    {
    }
}