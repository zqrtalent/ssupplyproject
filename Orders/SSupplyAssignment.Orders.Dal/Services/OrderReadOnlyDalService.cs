﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Order.Shared.Interface;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Dal.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using SSupplyAssignment.Orders.Dal.Context;
    using SSupplyAssignment.Orders.Dal.Mappings;
    using Unity;

    internal class OrderReadOnlyDalService : IOrderReadOnlyDalService
    {
		private readonly IUnityContainer _container;

		public OrderReadOnlyDalService(IUnityContainer container)
		{
			_container = container;
		}

		public async Task<Result<OrderModel>> GetByIdAsync(long id)
		{
			var result = new Result<OrderModel>();
			if (id <= 0)
			{
				return result;
			}

			result = await ExecuteAsync(async (ctx) =>
			{
				return (await ctx.Orders
					.Where(o => o.Id == id)
					.SingleOrDefaultAsync())
                    .MapToModel();
			});

			return result;
		}

        public async Task<Result<IReadOnlyCollection<OrderModel>>> GetByStatusAsync(OrderStatusType statusType, int offset, int pageSize = 100)
        {
			pageSize = Math.Min(pageSize, 100);
			offset = Math.Max(0, offset);

			return await ExecuteAsync<IReadOnlyCollection<OrderModel>>(async (ctx) =>
			{
				return (await ctx.Orders
					.Where(o => o.StatusTypeCode == statusType.ToString())
                    .Skip(offset)
                    .Take(pageSize)
                    .OrderBy(x => x.DateCreatedUTC)
                    .ToListAsync())
                    .Select(x => x.MapToModel())
                    .ToList();
			});
		}

        #region Private functions.

        private async Task<Result> ExecuteAsync(Func<IOrdersDbContext,Task> func)
        {
			var result = new Result();
			using (var ctx = CreateDbContext())
			{
                try
                {
				    await func(ctx);
                }
                catch(Exception e)
                {
					result.AddMessage(Result.MessageType.Error, e.Message);
                }
			}
			return result;
		}

		private async Task<Result<TData>> ExecuteAsync<TData>(Func<IOrdersDbContext, Task<TData>> func)
		{
			var result = new Result<TData>();
			using (var ctx = CreateDbContext())
			{
				try
				{
					result.Data = await func(ctx);
				}
				catch (Exception e)
				{
					result.AddMessage(Result.MessageType.Error, e.Message);
				}
			}
			return result;
		}

		private IOrdersDbContext CreateDbContext()
		{
			return _container.Resolve<IOrdersDbContext>();
		}
        #endregion
    }
}
