﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SSupplyAssignment.Common.Message.Order;
using SSupplyAssignment.EventBus.Shared;
using SSupplyAssignment.Orders.Shared.Managers;

namespace SSupplyAssignment.Orders.Api.Handlers
{
    using System.Linq;
    using Serilog.Context;
    using SSupplyAssignment.Orders.Shared.Models;

    public class UpdateOrderStatusMessageHandler : IEventMessageHandler<UpdateOrderStatusMessage>
    {
        private readonly IOrderManager _orderManager;
        private readonly ILogger<CreateOrderMessageHandler> _logger;

        public UpdateOrderStatusMessageHandler(IOrderManager orderManager, ILogger<CreateOrderMessageHandler> logger)
        {
            _orderManager = orderManager;
            _logger = logger;
        }

        public async Task Handle(UpdateOrderStatusMessage message)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{message.Id}-{Program._appName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", message.Id, Program._appName, message);
                var statusNew = Enum.Parse<OrderStatusType>(message.StatusTypeCode);
                var updateStatusResult = await _orderManager.UpdateOrderStatusAsync(message.OrderId, statusNew);
            }
        }
    }
}
