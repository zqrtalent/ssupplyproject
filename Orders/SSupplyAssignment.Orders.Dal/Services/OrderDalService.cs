﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Order.Shared.Interface;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Dal.Services
{
	using Entities = SSupplyAssignment.Orders.Dal.Entities;
	using SSupplyAssignment.Orders.Dal.Context;
    using SSupplyAssignment.Orders.Dal.Mappings;
    using Unity;

    internal class OrderDalService : IOrderDalService
    {
		private readonly IUnityContainer _container;

		public OrderDalService(IUnityContainer container)
		{
			_container = container;
		}

		public async Task<Result<OrderModel>> CreateAsync(OrderModel order)
        {
			if (order == null)
				throw new ArgumentNullException($"{nameof(order)}");
			return await ExecuteAsync(async (ctx) =>
			{
				var entity = order.MapToEntity(false);
				ctx.Orders.Add(entity);
				await ctx.SaveChangesAsync();
				return entity.MapToModel();
			});
		}

        public async Task<Result<Tuple<OrderModel,int>>> UpdateOrderStatusAsync(long id, OrderStatusType status)
        {
			return await ExecuteAsync(async (ctx) =>
			{
				var affectedRows = 0;
				var entity = await ctx.Orders.FindAsync(id);
				var model = entity.MapToModel();

                if(entity != null && entity.StatusTypeCode != status.ToString())
                {
					entity.StatusTypeCode = status.ToString();
					entity.DateLastUpdatedUTC = DateTime.UtcNow;
					ctx.Orders.Update(entity);
					affectedRows = await ctx.SaveChangesAsync();
				}

				return new Tuple<OrderModel, int>(model, affectedRows);
			});
		}

		private async Task<Result> ExecuteAsync(Func<IOrdersDbContext, Task> func)
		{
			var result = new Result();
			using (var ctx = CreateDbContext())
			{
				try
				{
					await func(ctx);
				}
				catch (Exception e)
				{
					result.AddMessage(e);
				}
			}
			return result;
		}

		private async Task<Result<TData>> ExecuteAsync<TData>(Func<IOrdersDbContext, Task<TData>> func)
		{
			var result = new Result<TData>();
			using (var ctx = CreateDbContext())
			{
				try
				{
					result.Data = await func(ctx);
				}
				catch (Exception e)
				{
					result.AddMessage(e);
				}
			}
			return result;
		}

		private IOrdersDbContext CreateDbContext()
		{
			return _container.Resolve<IOrdersDbContext>();
		}
	}
}
