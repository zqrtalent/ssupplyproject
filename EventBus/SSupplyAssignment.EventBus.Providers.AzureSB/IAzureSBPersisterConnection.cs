﻿using System;
using Microsoft.Azure.ServiceBus;

namespace SSupplyAssignment.EventBus.Providers.AzureSB
{
    public interface IAzureSBPersisterConnection : IDisposable
    {
        ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder { get; }

        ITopicClient CreateModel();
    }
}
