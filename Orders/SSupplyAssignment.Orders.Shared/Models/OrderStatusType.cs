﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SSupplyAssignment.Orders.Shared.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OrderStatusType
    {
        Created,

        Paid,

        Ready,

        History
    }
}
