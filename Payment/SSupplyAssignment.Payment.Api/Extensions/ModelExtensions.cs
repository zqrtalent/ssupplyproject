﻿using System.Collections.Generic;
using System.Linq;
using SSupplyAssignment.Payment.Api.Models;
using SSupplyAssignment.Payment.Shared.Models;

namespace SSupplyAssignment.Payment.Api.Extensions
{
    public static class ModelExtensions
    {
        public static PaymentApiModel ToApiModel(this PaymentModel model)
        {
            if (model == null) return null;

            return new PaymentApiModel
            {
                Id = model.Id,
                DateCreatedUTC = model.DateCreatedUTC,
                DateLastUpdatedUTC = model.DateLastUpdatedUTC,
                StatusType = model.StatusType,
                Amount = model.Amount,
                PaymentKey = model.PaymentKey,
                ProductItemCode = model.ProductItemCode,      
            };
        }

        public static IEnumerable<PaymentApiModel> ToApiModel(this IEnumerable<PaymentModel> model)
        {
            if (!(model?.Any() ?? false)) return null;
            return model.Select(x => x.ToApiModel()).ToList();
        }
    }
}
