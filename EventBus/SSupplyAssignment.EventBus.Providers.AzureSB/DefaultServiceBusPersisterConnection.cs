﻿using System;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;

namespace SSupplyAssignment.EventBus.Providers.AzureSB
{
    internal class DefaultAzureSBPersisterConnection : IAzureSBPersisterConnection
    {
        private readonly ILogger<DefaultAzureSBPersisterConnection> _logger;
        private readonly ServiceBusConnectionStringBuilder _serviceBusConnectionStringBuilder;
        private ITopicClient _topicClient;

        bool _disposed;

        public DefaultAzureSBPersisterConnection(ServiceBusConnectionStringBuilder serviceBusConnectionStringBuilder, ILogger<DefaultAzureSBPersisterConnection> logger)
        {
            _logger = logger;
            _serviceBusConnectionStringBuilder = serviceBusConnectionStringBuilder;
            _topicClient = new TopicClient(_serviceBusConnectionStringBuilder, RetryPolicy.Default);
        }

        public ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder => _serviceBusConnectionStringBuilder;

        public ITopicClient CreateModel()
        {
            if (_topicClient.IsClosedOrClosing)
            {
                _topicClient = new TopicClient(_serviceBusConnectionStringBuilder, RetryPolicy.Default);
            }

            return _topicClient;
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
        }
    }
}
