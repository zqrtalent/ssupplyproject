﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Common.Message.Payment
{
    public class PaymentSucceededMessage : EventMessage
    {
        public string ProductItemCode { get; set; }

        public string PaymentKey { get; set; }

        public decimal Amount { get; set; }
    }
}
