﻿using System;
namespace SSupplyAssignment.BO.Web.Settings
{
    public class BoWebSettings
    {
        public ApiEndpointSettings OrderApi { get; set; }

        public ApiEndpointSettings PaymentApi { get; set; }
    }
}
