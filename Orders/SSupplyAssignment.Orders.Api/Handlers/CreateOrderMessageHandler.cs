﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SSupplyAssignment.Common.Message.Order;
using SSupplyAssignment.EventBus.Shared;
using SSupplyAssignment.Orders.Shared.Managers;

namespace SSupplyAssignment.Orders.Api.Handlers
{
    using System.Linq;
    using Serilog.Context;
    using SSupplyAssignment.Orders.Shared.Models;

    public class CreateOrderMessageHandler: IEventMessageHandler<CreateOrderMessage>
    {
        private readonly IOrderManager _orderManager;
        private readonly ILogger<CreateOrderMessageHandler> _logger;

        public CreateOrderMessageHandler(IOrderManager orderManager, ILogger<CreateOrderMessageHandler> logger)
        {
            _orderManager = orderManager;
            _logger = logger;
        }

        public async Task Handle(CreateOrderMessage message)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{message.Id}-{Program._appName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", message.Id, Program._appName, message);

                var createResult = await _orderManager.CreateOrderAsync(new OrderModel
                {
                    CustomerId = message.CustomerId,
                    CustomerEmail = message.CustomerEmail,
                    ServiceId = message.ServiceId,
                    StatusType = OrderStatusType.Created,
                    ServiceAttributesJson = !(message.ServiceAttributes?.Any() ?? false)
                                            ? Newtonsoft.Json.JsonConvert.SerializeObject(message.ServiceAttributes)
                                            : null
                });
            }
        }
    }
}
