﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SSupplyAssignment.Payment.Dal.Entities
{
    [Table("Payment", Schema = "paymentdb")]
    public class Payment
    {
        [Key, Column(Order = 1)]
        public long Id { get; set; }

        [Column, StringLength(100)]
        public string PaymentKey { get; set; }

        [Column]
        public decimal Amount { get; set; }

        [Column, StringLength(1024)]
        public string ProductItemCode { get; set; }

        [Column, StringLength(50)]
        public string StatusTypeCode { get; set; }

        [Column]
        public DateTime DateLastUpdatedUTC { get; set; } = DateTime.UtcNow;

        [Column]
        public DateTime DateCreatedUTC { get; set; } = DateTime.UtcNow;
    }
}
