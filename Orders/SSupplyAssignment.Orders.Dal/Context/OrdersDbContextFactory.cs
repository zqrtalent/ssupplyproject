﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

/*
Add data migration: dotnet ef migrations add migration_name_here
Update database: dotnet ef database update
 */

namespace SSupplyAssignment.Orders.Dal.Context
{
    public class OrdersDbContextFactory : IDesignTimeDbContextFactory<OrdersDbContext>
    {
        public OrdersDbContext CreateDbContext(string[] args)
        {
            if(args != null)
            {
                foreach(var arg in args)
                {
                    Console.WriteLine(arg);
                }
            }

            var connectionString = "server=localhost;port=3307;userid=root;pwd=pass123;database=orderdb;sslmode=none;";
            var ob = new DbContextOptionsBuilder<OrdersDbContext>()
                .UseMySql(connectionString, mysqlOptionsbuildr => { mysqlOptionsbuildr.EnableRetryOnFailure(); } );

            return new OrdersDbContext(ob.Options, "orderdb");
        }
    }
}
