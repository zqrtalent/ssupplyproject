using System;

namespace SSupplyAssignment.BO.Web.Models
{
    public class ServiceViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
