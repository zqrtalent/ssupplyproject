﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.Payment.Api.Models.Request
{
    public class CreatePaymentRequestModel
    {
        [Required]
        public string PaymentKey { get; set; }

        [Required]
        public string ProductItemCode { get; set; }

        [Required]
        public decimal Amount { get; set; }
    }
}
