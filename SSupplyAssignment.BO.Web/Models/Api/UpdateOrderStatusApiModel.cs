﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.Orders.Api.Models.Request
{
    public class UpdateOrderStatusApiModel
    {
        [Required]
        public long OrderId { get; set; }

        [Required]
        public string StatusNew { get; set; }
    }
}
