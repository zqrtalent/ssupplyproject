﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Payment.Shared.Interface;
using SSupplyAssignment.Payment.Shared.Models;

namespace SSupplyAssignment.Payment.Dal.Services
{
	using Entities = SSupplyAssignment.Payment.Dal.Entities;
	using SSupplyAssignment.Payment.Dal.Context;
    using SSupplyAssignment.Payment.Dal.Mappings;
    using Unity;

    internal class PaymentDalService : IPaymentDalService
	{
		private readonly IUnityContainer _container;

		public PaymentDalService(IUnityContainer container)
		{
			_container = container;
		}

		public async Task<Result<PaymentModel>> CreateAsync(PaymentModel model)
        {
			if (model == null)
				throw new ArgumentNullException($"{nameof(model)}");
			return await ExecuteAsync(async (ctx) =>
			{
				var entity = model.MapToEntity(false);
				ctx.Payments.Add(entity);
				await ctx.SaveChangesAsync();
				return entity.MapToModel();
			});
		}

		private async Task<Result> ExecuteAsync(Func<IPaymentDbContext, Task> func)
		{
			var result = new Result();
			using (var ctx = CreateDbContext())
			{
				try
				{
					await func(ctx);
				}
				catch (Exception e)
				{
					result.AddMessage(e);
				}
			}
			return result;
		}

		private async Task<Result<TData>> ExecuteAsync<TData>(Func<IPaymentDbContext, Task<TData>> func)
		{
			var result = new Result<TData>();
			using (var ctx = CreateDbContext())
			{
				try
				{
					result.Data = await func(ctx);
				}
				catch (Exception e)
				{
					result.AddMessage(e);
				}
			}
			return result;
		}

		private IPaymentDbContext CreateDbContext()
		{
			return _container.Resolve<IPaymentDbContext>();
		}
	}
}
