﻿using System;
using Unity;

namespace SSupplyAssignment.Payment.Api.DiConfig
{
    using System.Linq;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using SSupplyAssignment.EventBus.Providers.RabbitMQ.DiConfig;
    using SSupplyAssignment.EventBus.Providers.RabbitMQ.Settings;
    using SSupplyAssignment.Payment.Api.Handlers;
    using SSupplyAssignment.Payment.Api.Models.Settings;
    using SSupplyAssignment.Payment.Dal.DiConfig;
    using SSupplyAssignment.Payment.Logic.DiConfig;
    using Unity.Injection;
    using Unity.Lifetime;

    public static class Config
    {
        public static IUnityContainer ConfigurePaymentApi(this IUnityContainer container)
        {
            var settings = container.Resolve<PaymentApiSettings>();
            var busSettings = settings.BusSettings;
            if(busSettings == null)
            {
                throw new ArgumentNullException($"{nameof(PaymentApiSettings.BusSettings)}");
            }

            if (settings.DatabaseSettings == null)
            {
                throw new ArgumentNullException($"{nameof(PaymentApiSettings.DatabaseSettings)}");
            }

            container
                .ConfigurePaymentDal(settings.DatabaseSettings)
                .ConfigurePaymentLogic()
                .ConfigureEventBusRabbitMQ(null, new RabbitMQBusSettings
                {
                    BusConnectionString = busSettings.BusConnectionString,
                    Password = busSettings.Password,
                    RetryCt = busSettings.RetryCt,
                    SubscriptionQueueName = busSettings.SubscriptionQueueName,
                    UserName = busSettings.Username
                });

            container
                .ConfigurePaymentApiHandlers()
                .ConfigureUnityLogging();

            return container;
        }

        public static IUnityContainer ConfigurePaymentApiHandlers(this IUnityContainer container)
        {
            // Register handlers.
            container.RegisterType<CreatePaymentMessageHandler>();
            return container;
        }

        public static IUnityContainer ConfigureUnityLogging(this IUnityContainer container)
        {
            container.RegisterInstance<ILoggerFactory>(new LoggerFactory().AddSerilog(), new ContainerControlledLifetimeManager());
            container.RegisterFactory(typeof(ILogger<>), null, (c, t, n) =>
            {
                var factory = c.Resolve<ILoggerFactory>();
                var genericType = t.GetGenericArguments().First();
                var mi = typeof(LoggerFactoryExtensions).GetMethods().Single(m => m.Name == "CreateLogger" && m.IsGenericMethodDefinition);
                var gi = mi.MakeGenericMethod(t.GetGenericArguments().First());
                return gi.Invoke(null, new[] { factory });
            });
            return container;
        }
    }
}
