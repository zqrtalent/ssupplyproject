﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SSupplyAssignment.Orders.Dal.Entities
{
    [Table("Orders", Schema = "orderdb")]
    public class Order
    {
        [Key, Column(Order = 1)]
        public long Id { get; set; }

        [Column]
        public long CustomerId { get; set; }

        [Column, StringLength(50)]
        public string CustomerEmail { get; set; }

        [Column]
        public long ServiceId { get; set; }

        [Column, StringLength(1024)]
        public string ServiceAttributesJson { get; set; }

        [Column, StringLength(50)]
        public string StatusTypeCode { get; set; }

        [Column]
        public DateTime DateLastUpdatedUTC { get; set; } = DateTime.UtcNow;

        [Column]
        public DateTime DateCreatedUTC { get; set; } = DateTime.UtcNow;
    }
}
