﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Common.Message.Order
{
    public class UpdateOrderStatusMessage : EventMessage
    {
        public long OrderId { get; set; }

        public string StatusTypeCode { get; set; }
    }
}
