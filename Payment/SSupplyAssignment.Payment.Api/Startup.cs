using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SSupplyAssignment.Payment.Api
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using SSupplyAssignment.Payment.Api.DiConfig;
    using SSupplyAssignment.Payment.Api.Extensions;
    using SSupplyAssignment.Payment.Api.Models.Settings;
    using Unity;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllersAsServices();
            services
                .AddControllers()
                .AddNewtonsoftJson(options =>
                 {
                     options.SerializerSettings.Converters.Add(new StringEnumConverter());
                     options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                     options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                     options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
                 });

            services.AddOptions();
            services.AddCors();

            services.Configure<PaymentApiSettings>(Configuration.GetSection(Program._mainSettingsSection));
            var settings = Configuration.GetSection(Program._mainSettingsSection).Get<PaymentApiSettings>();
            Program._container.RegisterInstance(typeof(PaymentApiSettings), null, settings, new Unity.Lifetime.SingletonLifetimeManager());

            Program._container.ConfigurePaymentApi();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseEventBus();
        }
    }
}
