﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Entities = SSupplyAssignment.Payment.Dal.Entities;

namespace SSupplyAssignment.Payment.Dal.Context
{
    public class PaymentDbContext : DbContext, IPaymentDbContext
    {

        private readonly Guid _id = Guid.NewGuid();
        private readonly string _schema;

        public PaymentDbContext(DbContextOptions<PaymentDbContext> options, string schema) : base(options)
        {
            _schema = schema;

#if DEBUG
            Console.WriteLine($"{nameof(PaymentDbContext)} -> new {_id.ToString("n")}");
#endif
        }

        ~PaymentDbContext()
        {
#if DEBUG
            Console.WriteLine($"{nameof(PaymentDbContext)} -> ~ {_id.ToString("n")}");
#endif
            //Dispose();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    _optionsBuilderAction(optionsBuilder);
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (!string.IsNullOrEmpty(_schema))
            {
                modelBuilder.HasDefaultSchema(_schema);
            }
        }

        public DbSet<Entities.Payment> Payments { get; set; }
    }
}
