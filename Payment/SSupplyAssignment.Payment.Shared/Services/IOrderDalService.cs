﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Payment.Shared.Models;

namespace SSupplyAssignment.Payment.Shared.Interface
{
    public interface IPaymentDalService
    {
        /// <summary>
        /// Create payment entry.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Payment model object.</returns>
        Task<Result<PaymentModel>> CreateAsync(PaymentModel model);
    }
}