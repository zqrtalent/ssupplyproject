﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Entities = SSupplyAssignment.Payment.Dal.Entities;

namespace SSupplyAssignment.Payment.Dal.Context
{
    public interface IPaymentDbContext : IDisposable
    {
        public DbSet<Entities.Payment> Payments { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
