﻿using System;
using SSupplyAssignment.Common.Shared.Settings;
using SSupplyAssignment.Core.Shared.Settings;

namespace SSupplyAssignment.Payment.Api.Models.Settings
{
    public class PaymentApiSettings
    {
        public DbConnectionSettings DatabaseSettings { get; set; }

        public EventBusSettings BusSettings { get; set; }
    }
}
