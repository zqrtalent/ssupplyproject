﻿using System;
namespace SSupplyAssignment.Common.Shared.Settings
{
    public class EventBusSettings
    {
        public string BusConnectionString { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string SubscriptionQueueName { get; set; }

        public int RetryCt { get; set; }
    }
}
