﻿using System;
using Newtonsoft.Json;

namespace SSupplyAssignment.EventBus.Shared.Model
{
    public class EventMessage
    {
        public EventMessage()
        {
            Id = Guid.NewGuid();
            CreatedUtc = DateTime.UtcNow;
        }

        [JsonConstructor]
        public EventMessage(Guid id, DateTime createdUtc)
        {
            Id = id;
            CreatedUtc = createdUtc;
        }

        [JsonProperty]
        public Guid Id { get; private set; }

        [JsonProperty]
        public DateTime CreatedUtc { get; private set; }
    }
}
