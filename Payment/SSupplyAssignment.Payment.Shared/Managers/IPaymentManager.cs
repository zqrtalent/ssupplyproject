﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Payment.Shared.Models;

namespace SSupplyAssignment.Payment.Shared.Managers
{
    public interface IPaymentManager
    {
        /// <summary>
        /// Creates new payment entry.
        /// </summary>
        /// <param name="model">Payment model</param>
        /// <returns></returns>
        Task<Result<PaymentModel>> CreatePaymentAsync(PaymentModel model);
    }
}