using System;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.BO.Web.Models
{
    public class PayViewModel
    {
        public long OrderId { get; set; }

        public decimal Amount { get; set; }
    }
}
