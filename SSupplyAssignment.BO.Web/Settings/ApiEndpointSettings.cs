﻿using System;
namespace SSupplyAssignment.BO.Web.Settings
{
    public class ApiEndpointSettings
    {
        public string Endpoint { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
