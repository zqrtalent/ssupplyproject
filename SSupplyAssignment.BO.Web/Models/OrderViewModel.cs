using System;
using System.ComponentModel.DataAnnotations;

namespace SSupplyAssignment.BO.Web.Models
{
    public class OrderViewModel
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerEmail { get; set; }

        public long ServiceId { get; set; }

        public string ServiceName { get; set; }

        public string ServiceAttributes { get; set; }

        [Display(Name = "Status")]
        public string StatusTypeCode { get; set; }

        public DateTime CreatedUtc { get; set; }

        public DateTime LastUpdatedUtc { get; set; }
    }
}
