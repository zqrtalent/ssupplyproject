﻿using System;
using Unity;

namespace SSupplyAssignment.EventBus.Providers.RabbitMQ.DiConfig
{
    using global::RabbitMQ.Client;
    using SSupplyAssignment.EventBus.Providers.RabbitMQ.Settings;
    using SSupplyAssignment.EventBus.Shared;

    public static class Config
    {
        public static IUnityContainer ConfigureEventBusRabbitMQ(this IUnityContainer container, string name, RabbitMQBusSettings settings)
        {
            container.RegisterInstance(name, settings);
            container.RegisterSingleton<IRabbitMQPersistentConnection, DefaultRabbitMQPersistentConnection>(name);
            container.RegisterSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>(name);
            container.RegisterSingleton<IEventBus, EventBusRabbitMQ>(name);
            return container;
        }
    }
}
