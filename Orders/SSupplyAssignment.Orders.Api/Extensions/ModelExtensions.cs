﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using SSupplyAssignment.Orders.Api.Models;
using SSupplyAssignment.Orders.Api.Models.Request;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Api.Extensions
{
    public static class ModelExtensions
    {
        public static OrderApiModel ToApiModel(this OrderModel model)
        {
            if (model == null) return null;

            return new OrderApiModel
            {
                Id = model.Id,
                CustomerEmail = model.CustomerEmail,
                CustomerId = model.CustomerId,
                DateCreatedUTC = model.DateCreatedUTC,
                DateLastUpdatedUTC = model.DateLastUpdatedUTC,
                ServiceAttributesJson = model.ServiceAttributesJson,
                ServiceId = model.ServiceId,
                StatusType = model.StatusType,
            };
        }

        public static IEnumerable<OrderApiModel> ToApiModel(this IEnumerable<OrderModel> model)
        {
            if (!(model?.Any() ?? false)) return null;

            return model.Select(x => x.ToApiModel()).ToList();
        }

        public static OrderModel ToModel(this CreateOrderRequestModel model)
        {
            if (model == null) return null;

            return new OrderModel
            {
                CustomerEmail = model.CustomerEmail,
                CustomerId = model.CustomerId,
                ServiceAttributesJson = (model.ServiceAttributes?.Count ?? 0) > 0 ?
                        JsonConvert.SerializeObject(model.ServiceAttributes) :
                        string.Empty,
                ServiceId = model.ServiceId,
            };
        }
    }
}
