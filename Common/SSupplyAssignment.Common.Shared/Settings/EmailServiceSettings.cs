﻿using System;
namespace SSupplyAssignment.Common.Shared.Settings
{
    public class EmailServiceSettings
    {
        public string ApiKey { get; set; }

        public string ApiId { get; set; }
    }
}
