﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Entities = SSupplyAssignment.Orders.Dal.Entities;

namespace SSupplyAssignment.Orders.Dal.Context
{
    public interface IOrdersDbContext : IDisposable
    {
        public DbSet<Entities.Order> Orders { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
