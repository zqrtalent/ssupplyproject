﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Payment.Api.Models.Message
{
    public class CreatePaymentMessage : EventMessage
    {
        public string PaymentKey { get; set; }

        public string ProductItemCode { get; set; }

        public decimal Amount { get; set; }

        public string PaymentStatusTypeCode { get; set; }
    }
}
