﻿using System;
using System.Threading.Tasks;
using SSupplyAssignment.Core.Shared.Models;
using SSupplyAssignment.Order.Shared.Interface;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Logic.Services
{
    using System.Collections.Generic;
    using SendGrid;
    using SendGrid.Helpers.Mail;
    using System.Linq;

    internal class SendgridEmailService : IEmailService
    {
        private readonly ISendGridClient _client;
        private readonly string _templateId = "d-453db5a2dcb044d1b69cd52be4d2410c";

        public SendgridEmailService(ISendGridClient client)
        {
            _client = client;
        }

        public async Task<Result> SendOrderIsReadyAsync(OrderModel order)
        {
            IDictionary<string, string> dynamicData = null;

            var msg = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(
                new EmailAddress("no-reply@test.com"),
                new[] { new EmailAddress(order.CustomerEmail) }.ToList(),
                _templateId,
                dynamicData);

            var result = new Result();
            try
            {
                var resp = await _client.SendEmailAsync(msg);
            }
            catch(Exception e)
            {
                result.AddMessage(e);
            }
            
            return result;
        }
    }
}
