﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Common.Message.Payment
{
    public class PaymentFailedMessage : EventMessage
    {
        public string ProductItemCode { get; set; }

        public decimal Amount { get; set; }

        public string ReasonCode { get; set; }
    }
}
