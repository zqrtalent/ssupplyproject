﻿using System;
using SSupplyAssignment.EventBus.Providers.AzureSB.Settings;
using SSupplyAssignment.EventBus.Shared;
using Unity;

namespace SSupplyAssignment.EventBus.Providers.AzureSB.DiConfig
{
    public static class Config
    {
        public static IUnityContainer ConfigureEventBusAzureSB(this IUnityContainer container, string name, AzureSBusSettings settings)
        {
            container.RegisterInstance(name, settings);
            container.RegisterSingleton<IAzureSBPersisterConnection, DefaultAzureSBPersisterConnection>(name);
            container.RegisterSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>(name);
            container.RegisterSingleton<IEventBus, EventBusAzureSB>(name);
            return container;
        }
    }
}
