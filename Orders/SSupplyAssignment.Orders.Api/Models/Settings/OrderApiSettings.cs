﻿using System;
using SSupplyAssignment.Common.Shared.Settings;
using SSupplyAssignment.Core.Shared.Settings;

namespace SSupplyAssignment.Orders.Api.Models.Settings
{
    public class OrderApiSettings
    {
        public DbConnectionSettings DatabaseSettings { get; set; }

        public EventBusSettings BusSettings { get; set; }

        public EventBusSettings PaymentBusSettings { get; set; }

        public EmailServiceSettings EmailSettings { get; set; }
    }
}
