﻿using System;
using System.Threading.Tasks;

namespace SSupplyAssignment.EventBus.Shared
{
    public interface IDynamicEventMessageHandler
    {
        Task Handle(dynamic message);
    }
}