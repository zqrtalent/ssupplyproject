﻿using System;
namespace SSupplyAssignment.Orders.Shared.Models
{
    public class OrderModel
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }

        public string CustomerEmail { get; set; }

        public long ServiceId { get; set; }

        public string ServiceAttributesJson { get; set; }

        public string StatusTypeCode
        {
            get
            {
                return StatusType.ToString();
            }
            set
            {
                var statusType = default(OrderStatusType);
                Enum.TryParse<OrderStatusType>(value, out statusType);
                StatusType = statusType;
            }
        }

        public OrderStatusType StatusType { get; set; } = OrderStatusType.Created;

        public DateTime DateLastUpdatedUTC { get; set; } = DateTime.UtcNow;

        public DateTime DateCreatedUTC { get; set; } = DateTime.UtcNow;
    }
}
