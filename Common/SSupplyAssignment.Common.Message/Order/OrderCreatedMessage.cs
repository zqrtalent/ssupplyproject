﻿using System;
using SSupplyAssignment.EventBus.Shared.Model;

namespace SSupplyAssignment.Common.Message.Order
{
    public class OrderCreatedMessage : EventMessage
    {
        public long OrderId { get; set; }

        public DateTime DateCreatedUtc { get; set; }
    }
}
