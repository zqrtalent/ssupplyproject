## SSupplySample

build and run containers using command **docker-compose up -d**

BackOffice web: http://0.0.0.0:5004

Orders api: http://0.0.0.0:5002

Payment web: http://0.0.0.0:5003

NOTE: postmen collection files are also available. please refer to folder **postman_files**.
