﻿using System;
namespace SSupplyAssignment.Core.Shared.Settings
{
    public class DbConnectionSettings
    {
        public string ConnectionString { get; set; }

        public string Schema { get; set; }

        public int CommandTimeout { get; set; }

        public bool ExponentialRetry { get; set; }
    }
}
