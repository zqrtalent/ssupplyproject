﻿using System;
using SSupplyAssignment.Payment.Shared.Managers;

namespace SSupplyAssignment.Payment.Logic.Managers
{
    using System.Threading.Tasks;
    using SSupplyAssignment.Common.Message.Payment;
    using SSupplyAssignment.Core.Shared.Models;
    using SSupplyAssignment.EventBus.Shared;
    using SSupplyAssignment.Payment.Shared.Interface;
    using SSupplyAssignment.Payment.Shared.Models;

    internal class PaymentManager : IPaymentManager
    {
        private readonly IPaymentDalService _dalService;
        private readonly IEventBus _eventBus;

        public PaymentManager(IPaymentDalService dalService, IEventBus eventBus)
        {
            _dalService = dalService;
            _eventBus = eventBus;
        }

        public async Task<Result<PaymentModel>> CreatePaymentAsync(PaymentModel model)
        {
            if(model == null)
            {
                throw new ArgumentNullException($"{nameof(model)}");
            }

            var result = new Result<PaymentModel>();
            var createResult = await _dalService.CreateAsync(model);
            result.Merge(createResult);
            result.Data = createResult.Data;

            if(result.Success && result.Data != null)
            {
                if(model.StatusType == PaymentStatusType.Failed)
                {
                    _eventBus.Publish(new PaymentFailedMessage
                    {
                        Amount = model.Amount,
                        ProductItemCode = model.ProductItemCode,
                        ReasonCode = "UNKNWN"
                    });
                }

                if (model.StatusType == PaymentStatusType.Succeed)
                {
                    _eventBus.Publish(new PaymentSucceededMessage
                    {
                        Amount = model.Amount,
                        PaymentKey = model.PaymentKey,
                        ProductItemCode = model.ProductItemCode
                    });
                }
            }

            return result;
        }
    }
}
