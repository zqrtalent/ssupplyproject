﻿using System;
using Unity;

namespace SSupplyAssignment.Orders.Api.DiConfig
{
    using System.Linq;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using SSupplyAssignment.Common.Shared;
    using SSupplyAssignment.EventBus.Providers.RabbitMQ.DiConfig;
    using SSupplyAssignment.EventBus.Providers.RabbitMQ.Settings;
    using SSupplyAssignment.Orders.Api.Handlers;
    using SSupplyAssignment.Orders.Api.Models.Settings;
    using SSupplyAssignment.Orders.Dal.DiConfig;
    using SSupplyAssignment.Orders.Logic.DiConfig;
    using Unity.Injection;
    using Unity.Lifetime;

    public static class Config
    {
        public static IUnityContainer ConfigureOrdersApi(this IUnityContainer container)
        {
            var settings = container.Resolve<OrderApiSettings>();
            var busSettings = settings.BusSettings;
            if(busSettings == null)
            {
                throw new ArgumentNullException($"{nameof(OrderApiSettings.BusSettings)}");
            }

            var paymentBusSettings = settings.PaymentBusSettings;
            if (paymentBusSettings == null)
            {
                throw new ArgumentNullException($"{nameof(OrderApiSettings.PaymentBusSettings)}");
            }

            if (settings.DatabaseSettings == null)
            {
                throw new ArgumentNullException($"{nameof(OrderApiSettings.DatabaseSettings)}");
            }

            container
                .ConfigureOrdersDal(settings.DatabaseSettings)
                .ConfigureOrdersLogic()
                .ConfigureEmailServices(settings.EmailSettings)
                .ConfigureEventBusRabbitMQ(null, new RabbitMQBusSettings
                {
                    BusConnectionString = busSettings.BusConnectionString,
                    Password = busSettings.Password,
                    RetryCt = busSettings.RetryCt,
                    SubscriptionQueueName = busSettings.SubscriptionQueueName,
                    UserName = busSettings.Username
                })
                .ConfigureEventBusRabbitMQ(EventBusConstants.PaymentBusInstanceName, new RabbitMQBusSettings
                {
                    BusConnectionString = paymentBusSettings.BusConnectionString,
                    Password = paymentBusSettings.Password,
                    RetryCt = paymentBusSettings.RetryCt,
                    SubscriptionQueueName = paymentBusSettings.SubscriptionQueueName,
                    UserName = paymentBusSettings.Username
                });

            container
                .ConfigureOrdersApiHandlers()
                .ConfigureUnityLogging();

            return container;
        }

        public static IUnityContainer ConfigureOrdersApiHandlers(this IUnityContainer container)
        {
            // Register handlers.
            container.RegisterType<CreateOrderMessageHandler>();
            container.RegisterType<UpdateOrderStatusMessageHandler>();
            container.RegisterType<OrderStatusUpdatedMessageHandler>();
            container.RegisterType<PaymentSucceededMessageHandler>();
            return container;
        }

        public static IUnityContainer ConfigureUnityLogging(this IUnityContainer container)
        {
            container.RegisterInstance<ILoggerFactory>(new LoggerFactory().AddSerilog(), new ContainerControlledLifetimeManager());
            container.RegisterFactory(typeof(ILogger<>), null, (c, t, n) =>
            {
                var factory = c.Resolve<ILoggerFactory>();
                var genericType = t.GetGenericArguments().First();
                var mi = typeof(LoggerFactoryExtensions).GetMethods().Single(m => m.Name == "CreateLogger" && m.IsGenericMethodDefinition);
                var gi = mi.MakeGenericMethod(t.GetGenericArguments().First());
                return gi.Invoke(null, new[] { factory });
            });
            return container;
        }
    }
}
