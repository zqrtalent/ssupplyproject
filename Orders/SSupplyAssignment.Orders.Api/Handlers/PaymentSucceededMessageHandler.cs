﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SSupplyAssignment.EventBus.Shared;
using SSupplyAssignment.Orders.Shared.Managers;

namespace SSupplyAssignment.Orders.Api.Handlers
{
    using SSupplyAssignment.Common.Message.Payment;
    using SSupplyAssignment.Orders.Shared.Models;

    public class PaymentSucceededMessageHandler : IEventMessageHandler<PaymentSucceededMessage>
    {
        private readonly IOrderManager _orderManager;
        private readonly ILogger<CreateOrderMessageHandler> _logger;

        public PaymentSucceededMessageHandler(IOrderManager orderManager, ILogger<CreateOrderMessageHandler> logger)
        {
            _orderManager = orderManager;
            _logger = logger;
        }

        public async Task Handle(PaymentSucceededMessage message)
        {
            _logger.LogInformation("Handling : {Message} {AppName} {messageId} ", nameof(PaymentSucceededMessage), Program._appName, message.Id);

            var statusNew = OrderStatusType.Paid;
            long orderId = 0;
            long.TryParse(message.ProductItemCode, out orderId);
            if(orderId > 0)
            {
                var updateStatusResult = await _orderManager.UpdateOrderStatusAsync(orderId, statusNew);
            }
        }
    }
}
