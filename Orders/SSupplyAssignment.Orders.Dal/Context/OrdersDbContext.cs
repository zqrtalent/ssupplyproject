﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Entities = SSupplyAssignment.Orders.Dal.Entities;

namespace SSupplyAssignment.Orders.Dal.Context
{
    public class OrdersDbContext : DbContext, IOrdersDbContext
    {

        private readonly Guid _id = Guid.NewGuid();
        private readonly string _schema;

        public OrdersDbContext(DbContextOptions<OrdersDbContext> options, string schema) : base(options)
        {
            _schema = schema;

#if DEBUG
            Console.WriteLine($"{nameof(OrdersDbContext)} -> new {_id.ToString("n")}");
#endif
        }

        ~OrdersDbContext()
        {
#if DEBUG
            Console.WriteLine($"{nameof(OrdersDbContext)} -> ~ {_id.ToString("n")}");
#endif
            //Dispose();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    _optionsBuilderAction(optionsBuilder);
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (!string.IsNullOrEmpty(_schema))
            {
                modelBuilder.HasDefaultSchema(_schema);
            }
        }

        public DbSet<Entities.Order> Orders { get; set; }
    }
}
