﻿using System;
using SSupplyAssignment.Orders.Shared.Models;

namespace SSupplyAssignment.Orders.Api.Models
{
    public class OrderApiModel
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }

        public string CustomerEmail { get; set; }

        public long ServiceId { get; set; }

        public string ServiceAttributesJson { get; set; }

        public OrderStatusType StatusType { get; set; }

        public DateTime DateLastUpdatedUTC { get; set; }

        public DateTime DateCreatedUTC { get; set; }
    }
}
